# ctns simulator

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

COVID simulator and network evolution introduce a tool architecture of out networked SEIR model, keeping separate  the simulation process from the visualization. 
The simulator allows us to start the SARS-CoV-2 contact simulator, which has different initial parameters that it is possible to customize.

### Implementation
The entire application has been developed using python. In particular, the main libraries used are: *igraph*, *dash* and *ctns*.
In particular [ctns (Contact Tracing Network Simulator)](https://gitlab.com/mistrello96/ctns) is a tool to simulate digital contact networks beetween people in a population where a disease is spreading. The simulation is highly customizable and will return (or dump) a time series of networks.

The application has the following structure:
```
.                                                  
├ src
    |-app.py
    |-callbacks.py
    |-layouts.py                                             
├ simulator_results
├ simulator_app.py
├ .gitignore
├ requirements.txt
└ README.md
```
It is important to remark that the results of all simulation is saved in the *pickle* format and it is possible visualize it into the simultator_results folder.

### Installation
the use of the application, in the online demo version does not require any installation. This version may present limitation and it is not recommended for large analysis. The advantage of the online version is that the interface is immediately availble into the horoku server: [simulator](https://mind-lab-networked-covid-sim.herokuapp.com/)  without the need to install components. However, in this version the number of family and step are limited at 150 for not exceeding heroku timeout.

You can also download the application for local use [click here](https://gitlab.com/migliouni/ctns_simulator) . In this case you need to install locally some tools.

The first libraries is [igraph](https://igraph.org/python/#docs). It is a collection of libraries for graph creation and manipulation and network analysis.
```sh
$ pip install python-igraph
```
For the visualization part it is necessary to install [plotly dash](https://pypi.org/project/dash/)
```sh
$ pip install dash
```
simulator uses the [ctns library](https://gitlab.com/mistrello96/ctns)
```sh
$ pip install ctns
```
More easily, you can run the following command to install all the libraries required by the application
```sh
pip install -r requirements.txt
```
after installing all the libraries, to run the program locally, you must launch it through the command
```sh
$ python simulator_app.py
```
At the end of the launch, it is possible to see the following output
```sh
Running on http://127.0.0.1:8000/
Debugger PIN: 731-411-605
 * Serving Flask app "simulator_app" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: on
```
At this point, you must copy the link above and past it into a browser.

It is recommended the last version of python 3.x [click here for the download](https://www.python.org/downloads/).
#### Usage
The software is fully usable through the user interface. It makes the application very intuitive and easy to use. Into the Covid-19 contact simulator it is possible to set different parameters. 
The execution time depends on the parameters that have been set. However, the default parameters have been set up in such a way that the simulation is very fast and real. For example, setting the number of families grater than 1000, could significantly increase the computation time. Now, we provide a brief description for each parameter.
<img alt="Parameters that are possible to set" title="Parameters" src="https://imgur.com/VADf3Oo.png" width="450">


## Simulation 


#### Parameters 
Into the simulation process we distinguish two different type of parameters:
##### Initial parameters
* **Number of families** The number of families involved in the simulation. A family is a group of people that live together.
* **Number of initial exposed people** Usually called patient zero. This parameter represents the number of the people that are infected at the beginning of the simulation.
* **Simulation days** An integer number that represents the number of days of the simulation.
* **Incubation days** This is the first epidemic parameter and it represents a mean of the number of days of incubation.
* **Disease duration** the avarage duration of the Covid-19 disease.
* **R0** It is a decimal parameter and it is a mathematical term that indicates how contagious an infectious disease is.
* **Intial day restriction** This is the first sociability restriction parameter and it represents the day the restriction begins.
* **Duration of restriction** It represents the number of days which the restriction is active.
* **sociability distance strictness** This parameter is a percentile that you can set through a slidebar. If this parameter is equal to 0%, it means that no sociability distance has been adopted, on the contrary, the strictness of the sociability distance is very high.
* **Decreasing restrionction** If enabled, the restriction decrease with the evolution of the simulation.
* **Daily number of test** This parameter represents the number of tests that are carried out daily.
* **Policy test** The policy under which the tests are carried out. You can choose 3 different options: random, throut a computation of degree centrality or with a computation of between centrality (it may require more time for huge simulations).
* **Contact tracing efficiency**  the efficiency of contract tracing. This is a percentile which can be set through a slidebar.

The initial parameters have been grouped into four different parts in order to facilitate the use of the simulator
##### Output components
* **Simulation results** This part consistes of two different lineplots which represents the evolution of SEIRD model in the case we have adopted restriction and we have not adopeted restriction. In both graphs we have the count of people involved in the simulation on the y-axis and the number of days in the x-axis
Comparison Results - these lineplots compare the number of people infected with the total (first graph) and the number of dead with the total (second graph). The blue line represents the case which no restriction has been adopted. On the contraruy, orange line represents the case which restriction has been applied
Daily results with and without restriction - These lineplots are similar to the previous but they consider a daily comparison of infected.
<img alt="Example of Simulation results" title="Simulation results" src="https://imgur.com/ePZL2fn.png" width="500">

* **Comparison results** This barplot links the total number of infected in case if or not rectriction has been adopted.
<img alt="Example of Comparison results" title="Comparison results" src="https://imgur.com/dN9mjJF.png" width="800">

* **Daily results with and without restriction** These lineplots are similar to the previous but they consider a daily comparison of infected
<img alt="Example of Dailyresults" title="Daily results" src="https://imgur.com/wIZgl0g.png" width="800">

* **Total infected with and without restriction**  This barplot links the total number of infected in case if or not rectriction has been adopted
<img alt="Example of Dailyresults" title="Daily results" src="https://imgur.com/mtBynGJ.png" width="500">

#### Statistics tab
This tab contains some useful statistics used to develop the simulator, according to official data collected by the main Italian statistical associations. 

<img alt="Example of Dailyresults" title="Daily results" src="https://imgur.com/T7laZh5.png" width="850">
